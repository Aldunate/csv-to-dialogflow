from google.cloud import dialogflow as df
import os


list_id = ["prueba-297916","prueba2-297918","projecto-de-testeo-3","proyecto-smalltalk"]
os.environ["GOOGLE_APPLICATION_CREDENTIALS"] =  f'{list_id[0]}.json' 



def set_credentials(project_id):
    os.environ["GOOGLE_APPLICATION_CREDENTIALS"] =  f'{project_id}.json'
    
def get_intents(project_id):
    intents = []
    #set_credentials(project_id)
    intent_client = df.IntentsClient()
    intent_parent = df.AgentsClient.agent_path(project_id)
    for element in intent_client.list_intents(parent = intent_parent):
        intents.append(element.display_name)
    return intents
#get_intents("prueba-297916")

def get_training_phrases (project_id, intent, join = True):
    check_token = get_intents(project_id)
    if check_token.count(intent) == 0:
        return "invalid intent"
    phrases = []
    intent_client = df.IntentsClient()
    intent_parent = df.AgentsClient.agent_path(project_id)
    intent_id = get_intent_id(project_id, intent)
    if intent_id == 0:
        return "display_name not found"
    intent_path = intent_client.intent_path(project_id, intent_id)
    intent_request = df.GetIntentRequest(name = intent_path, intent_view= df.IntentView.INTENT_VIEW_FULL)
    intent = intent_client.get_intent(request = intent_request)
    element = intent
    for training_phrase in element.training_phrases:
        sentence = ""
        if join == True:
            for text in training_phrase.parts:
                sentence += text.text
            phrases.append(sentence)
        else:
            sentence = []
            for text in training_phrase.parts:                   
                if text.entity_type != '':
                    sentence.append({'text': text.text, 'entity_type': text.entity_type, 'alias': text.alias })
                else:
                    sentence.append({'text': text.text})
            phrases.append(sentence)
    return phrases



def get_chatbot_answer (project_id, intent):
    get_intents(project_id)
    check_token = get_intents(project_id)
    if check_token.count(intent) == 0:
        return "invalid intent"
    answer = []
    intent_client = df.IntentsClient()
    intent_parent = df.AgentsClient.agent_path(project_id)
    for element in intent_client.list_intents(parent = intent_parent):      
        if element.display_name == intent:
            if len(element.messages) != 0:
                for number in range(len(element.messages)):
                    lists = []
                    for chatbot_answer in element.messages[number].text.text:
                        lists.append(chatbot_answer)
                    answer.append(lists)
    return answer
#get_chatbot_answer (list_id[0], "Prueba2")



def get_input_context (project_id, intent):
    check_token = get_intents(project_id)
    if check_token.count(intent) == 0:
        return "invalid intent"
    context_names = []
    intent_client = df.IntentsClient()
    intent_parent = df.AgentsClient.agent_path(project_id)
    for element in intent_client.list_intents(parent =intent_parent):
        if element.display_name == intent:
            if (len(element.input_context_names) > 0):
                for context in element.input_context_names:
                    context_names.append(context.split("/")[-1])              
            else:
                return "no input context found"
            return context_names
#get_input_context (list_id[0], "Pasear perros")   




def get_output_context (project_id,intent):
    check_token = get_intents(project_id)
    if check_token.count(intent) == 0:
        return "invalid intent"
    context_names = []
    intent_client = df.IntentsClient()
    intent_parent = df.AgentsClient.agent_path(project_id)
    for element in intent_client.list_intents(parent = intent_parent):
        if element.display_name == intent:
            if (len(element.output_contexts) > 0):
                for contextname in element.output_contexts:
                    context_names.append(contextname.name.split("/")[-1])                            
            else:
                return "no input context found"
            return context_names
#get_output_context (list_id[0], "Pasear perros")    

def get_output_context_lifespan(project_id,intent,output_context):
    check_token = get_intents(project_id)
    if check_token.count(intent) == 0:
        return "invalid intent"
    life_span = 0
    intent_client = df.IntentsClient()
    intent_parent = df.AgentsClient.agent_path(project_id)
    for element in intent_client.list_intents(parent = intent_parent):
        if element.display_name == intent:
            if (len(element.output_contexts) > 0):
                for contextname in element.output_contexts:
                    if (contextname.name.split("/")[-1] == output_context):
                        life_span = contextname.lifespan_count
            else:
                return "no input context found"
            if life_span == 0:
                return "output_context not found"          
            return life_span
#get_output_context_lifespan(list_id[0], "Pasear perros", 'de-quien-es')

def get_intent_with_inputcontext(project_id,input_context):
    get_intents(project_id)
    intents = []
    intent_client = df.IntentsClient()
    intent_parent = df.AgentsClient.agent_path(project_id)
    for element in intent_client.list_intents(parent = intent_parent): 
        if len(element.input_context_names) > 0:
            for context in element.input_context_names:
                if (context.split("/")[-1] == input_context):
                    intents.append(element.display_name)
    if (len(intents) == 0):
        return "no intents have this input_context"
    return intents

#get_intent_with_inputcontext(list_id[0],'como-es')
def get_intent_with_outputcontext(project_id,output_context):
    get_intents(project_id)
    intents = []
    intent_client = df.IntentsClient()
    intent_parent = df.AgentsClient.agent_path(project_id)
    for element in intent_client.list_intents(parent = intent_parent): 
        if len(element.output_contexts) > 0:
            for context in element.output_contexts:
                if output_context == context.name.split("/")[-1]:                
                    intents.append(element.display_name)
    if(len(intents) == 0):
        return "no intents have this output_context"
    return intents
#get_intent_with_outputcontext(list_id[0],'de-quien-es')

def get_intent_id(project_id, display_name):
    intent_id = 0
    intents_client = df.IntentsClient()
    intent_parent = df.AgentsClient.agent_path(project_id)
    for element in intents_client.list_intents(parent = intent_parent):
        if element.display_name == display_name:
            intent_id = element.name.split("/")[-1]
    return intent_id
#get_intent_id(list_id[0], "Pasear perros")


def new_intent (project_id, display_name):
    get_intents(project_id)
    intents_client = df.IntentsClient()
    intent_parent = df.AgentsClient.agent_path(project_id)
    check_token = get_intents(project_id)
    if check_token.count(display_name) > 0:
        return  
    intent = df.Intent(display_name=display_name)
    intents_client.create_intent(parent =intent_parent, intent = intent)
    return
#new_intent (list_id[0], "nueva prueba")


def set_new_display_name(project_id, display_name ,new_display_name):
    list_intents = get_intents(project_id)
    if list_intents.count(new_display_name) > 0:
        return "Intents already exist"
    intent_client = df.IntentsClient()
    intent_parent = df.AgentsClient.agent_path(project_id)
    intent_id = get_intent_id(project_id, display_name)
    if intent_id == 0:
        return "display_name not found"
    intent_path = intent_client.intent_path(project_id, intent_id)
    intent = intent_client.get_intent(name = intent_path)
    intent.display_name = new_display_name
    intent_client.update_intent(intent = intent)
    return
    
#set_new_display_name("prueba-297916", "nueva prueba" ,"nueva prueba2")

def add_training_phrases(project_id,display_name ,new_training_phrases ): 
    get_intents(project_id)
    intent_client = df.IntentsClient()
    intent_parent = df.AgentsClient.agent_path(project_id)
    intent_id = get_intent_id(project_id, display_name)  
    if intent_id == 0:
        return "display_name not found"
    intent_path = intent_client.intent_path(project_id, intent_id)
    
    intent_request = df.GetIntentRequest(name = intent_path, intent_view= df.IntentView.INTENT_VIEW_FULL)
    intent = intent_client.get_intent(request = intent_request)
    training_phrases = []
    part = []
    for training_phrases_part in new_training_phrases:    
        if len(training_phrases_part) != 1:
            part.append(df.Intent.TrainingPhrase.Part(text=training_phrases_part['text'] , entity_type = training_phrases_part['entity_type'], user_defined = True, alias = training_phrases_part['alias'] ))
        else:
            part.append( df.Intent.TrainingPhrase.Part(text=training_phrases_part['text'], user_defined = True))
    training_phrase = df.Intent.TrainingPhrase(parts=part)
    training_phrases.append(training_phrase)
    intent.training_phrases.extend(training_phrases)
    intent_client.update_intent(intent = intent)
    return
    
#add_training_phrases("prueba-297916","olas" ,[ {'text': "lukas", 'entity':"@sys.ordinal" } ,{'text': "lukardio22", 'entity':"@sys.ordinal" }])

def add_responses(project_id,display_name,message_texts,number_response = 0):
    get_intents(project_id)
    intent_client = df.IntentsClient()
    intent_parent = df.AgentsClient.agent_path(project_id)
    intent_id = get_intent_id(project_id, display_name)
    if intent_id == 0:
        return "display_name not found"
    intent_path = intent_client.intent_path(project_id, intent_id)
    intent_request = df.GetIntentRequest(name = intent_path, intent_view= df.IntentView.INTENT_VIEW_FULL)
    intent = intent_client.get_intent(request = intent_request)
    
    for number in range(number_response):
        first_text = df.Intent.Message.Text(text = [""] )
        first_message = [df.Intent.Message(text = first_text )]
        intent.messages.extend(first_message)
        intent_client.update_intent(intent =intent)
    
    
    if len(intent.messages) == 0:
        first_text = df.Intent.Message.Text(text = [message_texts] )
        first_message = [df.Intent.Message(text = first_text )]
        intent.messages.extend(first_message)
        intent_client.update_intent(intent =intent)
    else:
        for response in intent.messages[number_response].text.text:
            if(response == message_texts ):
                return "La respuesta ya existe" 
        
        intent.messages[number_response].text.text.append(message_texts)
        intent_client.update_intent(intent = intent)
        return
    
#add_responses("prueba-297916", "Prueba2" ,"termino prueba10",number_response = 0)

def add_input_context(project_id,display_name,new_context_name):
    get_intents(project_id)
    current_contexts = get_input_context(project_id,display_name)
    if current_contexts.count(new_context_name)> 0:
        return "context already in this intent"
    intent_client = df.IntentsClient()
    intent_parent = df.AgentsClient.agent_path(project_id)
    intent_id = get_intent_id(project_id, display_name)
    if intent_id == 0:
        return "display_name not found"
    intent_path = intent_client.intent_path(project_id, intent_id)
    intent = intent_client.get_intent(name = intent_path)
    intent.input_context_names.extend([f'projects/{project_id}/agent/sessions/-/contexts/{new_context_name}'])
    intent_client.update_intent(intent = intent)
    return
#add_input_context("prueba-297916", "nueva prueba2" ,"newinputcontext")


def add_output_context(project_id,display_name, new_context_name, new_context_lifespam = 5):
    get_intents(project_id)
    current_contexts = get_output_context(project_id,display_name)
    if current_contexts.count(new_context_name)> 0:
        return "context already in this intent"
    intent_client = df.IntentsClient()
    intent_parent = df.AgentsClient.agent_path(project_id)
    intent_id = get_intent_id(project_id, display_name)
    if intent_id == 0:
        return "display_name not found"
    intent_path = intent_client.intent_path(project_id, intent_id)
    intent = intent_client.get_intent(name = intent_path)
    new_context = [df.Context(name=f'projects/{project_id}/agent/sessions/-/contexts/{new_context_name}',lifespan_count=new_context_lifespam)]                   
    intent.output_contexts.extend(new_context)
    intent_client.update_intent(intent =intent)
    return
#add_output_context("prueba-297916", "nueva prueba2" ,"newoutputcontext")


def delete_intent(project_id,display_name):  # no hecha por mi
    intents_client = df.IntentsClient()
    intent_id = get_intent_id(project_id, display_name)
    if intent_id == 0:
        return "display_name not found"
    intent_path = intents_client.intent_path(project_id, intent_id)
    intents_client.delete_intent(name = intent_path)
    return

#delete_intent("prueba-297916", "nueva prueba2")


def delete_training_phrase(project_id,display_name, frase_to_delete): #############################################################################
    get_intents(project_id)
    intent_client = df.IntentsClient()
    intent_parent = df.AgentsClient.agent_path(project_id)
    intent_id = get_intent_id(project_id, display_name)  
    if intent_id == 0:
        return "display_name not found"
    intent_path = intent_client.intent_path(project_id, intent_id)  
    intent_request = df.GetIntentRequest(name = intent_path, intent_view= df.IntentView.INTENT_VIEW_FULL)
    intent = intent_client.get_intent(request = intent_request)
    for a in intent.training_phrases:
        if a.parts[0].text == frase_to_delete:
            intent.training_phrases.remove(a)
    intent_client.update_intent(intent = intent)
    return
#delete_training_phrase("prueba-297916","olas" ,"hola que tales?")
def delete_all_training_phrases(project_id,display_name): #############################################################################
    get_intents(project_id)
    intent_client = df.IntentsClient()
    intent_parent = df.AgentsClient.agent_path(project_id)
    intent_id = get_intent_id(project_id, display_name)  
    if intent_id == 0:
        return "display_name not found"
    intent_path = intent_client.intent_path(project_id, intent_id)  
    intent_request = df.GetIntentRequest(name = intent_path, intent_view= df.IntentView.INTENT_VIEW_FULL)
    intent = intent_client.get_intent(request = intent_request)
    for a in intent.training_phrases:
        intent.training_phrases.remove(a)
    intent_client.update_intent(intent = intent)
    return

def delete_response(project_id, display_name, response_to_delete, number_response) :
    get_intents(project_id)
    intent_client = df.IntentsClient()
    intent_parent = df.AgentsClient.agent_path(project_id)
    intent_id = get_intent_id(project_id, display_name)
    if intent_id == 0:
        return "display_name not found"
    intent_path = intent_client.intent_path(project_id, intent_id)
    intent_request = df.GetIntentRequest(name = intent_path, intent_view= df.IntentView.INTENT_VIEW_FULL)
    intent = intent_client.get_intent(request = intent_request)
    for response in intent.messages[number_response].text.text:
        if(response == response_to_delete ):
            intent.messages[number_response].text.text.remove(response_to_delete)       
    intent_client.update_intent(intent = intent)
    return
#delete_response("prueba-297916", "nueva prueba2" ,"termino prueba")

def delete_all_responses(project_id, display_name) :
    get_intents(project_id)
    intent_client = df.IntentsClient()
    intent_id = get_intent_id(project_id, display_name)
    if intent_id == 0:
        return "display_name not found"
    intent_path = intent_client.intent_path(project_id, intent_id)
    intent_request = df.GetIntentRequest(name = intent_path, intent_view= df.IntentView.INTENT_VIEW_FULL)
    intent = intent_client.get_intent(request = intent_request)
    for i in range(len(intent.messages)):
        for response_to_delete in intent.messages[i].text.text:
            intent.messages[i].text.text.remove(response_to_delete)     
    intent_client.update_intent(intent = intent)
    return

def delete_input_context(project_id,display_name,input_context_name):
    get_intents(project_id)
    intent_client = df.IntentsClient()
    intent_parent = df.AgentsClient.agent_path(project_id)
    intent_id = get_intent_id(project_id, display_name)
    if intent_id == 0:
        return "display_name not found"
    current_contexts = get_input_context (project_id, display_name)
    if input_context_name not in current_contexts:
         return "input context not found" 
    intent_path = intent_client.intent_path(project_id, intent_id)
    intent = intent_client.get_intent(name = intent_path)
    intent.input_context_names.remove(f'projects/{project_id}/agent/sessions/-/contexts/{input_context_name}')
    intent_client.update_intent(intent = intent)  
    return
#delete_input_context("prueba-297916", "nueva prueba2" ,"newinputcontext")


def delete_output_context(project_id,display_name,output_context_name):
    get_intents(project_id)
    intent_client = df.IntentsClient()
    intent_parent = df.AgentsClient.agent_path(project_id)
    intent_id = get_intent_id(project_id, display_name)
    if intent_id == 0:
        return "display_name not found"
    current_contexts = get_output_context(project_id, display_name)
    if output_context_name not in current_contexts:
        return "output context not found" 
    intent_path = intent_client.intent_path(project_id, intent_id)
    intent = intent_client.get_intent(name = intent_path)
    for context in intent.output_contexts:
        if context.name == f'projects/{project_id}/agent/sessions/-/contexts/{output_context_name}':
            intent.output_contexts.remove(context)
    intent_client.update_intent(intent = intent)  
    return
#delete_output_context("prueba-297916", "nueva prueba2" ,"newoutputcontext")

def get_parameters(project_id,display_name):  
    intent_client = df.IntentsClient()
    intent_parent = df.AgentsClient.agent_path(project_id)
    intent_id = get_intent_id(project_id, display_name)  
    if intent_id == 0:
        return "display_name not found"
    intent_path = intent_client.intent_path(project_id, intent_id)
    intent_request = df.GetIntentRequest(name = intent_path, intent_view= df.IntentView.INTENT_VIEW_FULL)
    intent = intent_client.get_intent(request = intent_request)   
    return intent.parameters

#get_parameters("prueba-297916", "Cambio titular perro")


def change_parameters(project_id,display_name, parameter_name , new_mandatory = "0", new_promp = 0):
    intent_client = df.IntentsClient()
    intent_parent = df.AgentsClient.agent_path(project_id)
    intent_id = get_intent_id(project_id, display_name)  
    if intent_id == 0:
        return "display_name not found"
    intent_path = intent_client.intent_path(project_id, intent_id)
    intent_request = df.GetIntentRequest(name = intent_path, intent_view= df.IntentView.INTENT_VIEW_FULL)
    intent = intent_client.get_intent(request = intent_request) 
    
    aux = 0
    for data in intent.parameters:       
        if data.display_name == parameter_name:
            if new_mandatory != "0":
                intent.parameters[aux].mandatory = new_mandatory
            if new_promp != 0:
                intent.parameters[aux].prompts = [new_promp]   
        aux +=1          
    intent_client.update_intent(intent = intent)
    return


#change_parameters("prueba-297916", "Cambio titular perro", "date-time" , new_mandatory = True , new_promp = "obligatorio")




